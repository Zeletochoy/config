#! /bin/sh

echo "Clone the config repo..."
sudo apt-get update
sudo apt-get install -y git
git clone https://Zeletochoy@bitbucket.org/Zeletochoy/config.git ~
rm ~/install.sh

echo "Setup zsh..."
sudo apt-get install -y zsh curl
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
echo "source ~/.zshrc.custom" >> ~/.zshrc

echo "Setup i3..."
sudo apt-get install -y i3 python-pip
sudo pip install py3status

echo "Setup vim..."
sudo apt-get install -y vim
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
# TODO setup fonts for airline
